export const CONTROLLER_ROUTES = "controller:routes";
export const ROUTES_PREFIX = "$route_";
export const APP_CONTROLLERS = "app:controllers";
export const APP_SERVICES = "app:services";
export const APP_PLUGINS = "app:plugins";
